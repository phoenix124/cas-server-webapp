Cas-server-webapp
============

项目介绍:Cas 4.0 二次开发
---------------
        开发环境: Spring Tools Suits JDK1.7
        




#文档

1、详细请参看： 《Cas Server 4.0 二次开发说明文档》
2、原作者地址为：https://github.com/ameizi/cas-server-webapp

后续需要补充的内容：
1、客户端接入需要添加的内容
2、客户端测试接入示例
