/*
Navicat MySQL Data Transfer

Source Server         : 10.10.1.52
Source Server Version : 50713
Source Host           : 10.10.1.52:3306
Source Database       : mybatis-plus

Target Server Type    : MYSQL
Target Server Version : 50713
File Encoding         : 65001

Date: 2017-07-27 17:26:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sys_t_users`
-- ----------------------------
DROP TABLE IF EXISTS `sys_t_users`;
CREATE TABLE `sys_t_users` (
  `id` int(22) NOT NULL AUTO_INCREMENT,
  `loginname` varchar(100) DEFAULT NULL,
  `pwd` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_t_users   deane/123456
-- ----------------------------
INSERT INTO `sys_t_users` VALUES ('1', 'deane', 'e10adc3949ba59abbe56e057f20f883e');
INSERT INTO `sys_t_users` VALUES ('2', 'admin', 'e10adc3949ba59abbe56e057f20f883e');
